## Synopsis

Xeoncfg provides a configuration package for Xeon phones, which are based on the PiBox distribution with custom apps running on Raspberry Pi hardware and touchscreen displays.

Xeoncfg implements core platform changes required for use with Xeon phones.  It does not have any interface and is only run at package installation time.

## Build

### Cross compile and packaging

To cross compile the application run the following command.

    sudo make pkg

## Installation

Xeoncfg is packaged in an opkg format.  After building look in the pkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/xeoncfg_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/xeoncfg_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

